/** 
 *  * @file 
 *   * @author Rahimian, Abtin <arahimian@acm.org> 
 *    * @revision $Revision: 11 $ 
 *     * @tags $Tags: tip $ 
 *      * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $ 
 *       * 
 *        * @brief 
 *         */

 
#include <cstdio> 
#include <cstdlib> 
#include <cassert> 
#include <unistd.h>  
#include "omp-utils.h" 
#include "semaphore.h" 
 
#define BSZ 5 
 
int buffer[BSZ]; 
int next_put = 0; 
int next_get = 0; 
 
omp_lock_t put_lock_;  
omp_lock_t get_lock_;  
Sem empty(BSZ, "empty");  
Sem filled(0, "filled");  
void put(int value) { 
    int idx; 
    idx = next_put; 
    next_put = (next_put + 1) % BSZ; 
    buffer[idx] = value; 
} 
 
int get() { 
    int idx; 
    idx = next_get; 
    next_get = (next_get + 1) % BSZ; 
    return buffer[idx]; 
} 
 
void* producer(int nprod, int rank) { 
    TID("Starting producer"); 
 
    for (int i = 0; i < nprod; i++){       
empty.wait(); 
omp_set_lock(&put_lock_);  
       put(nprod*rank+i+1); 
omp_unset_lock(&put_lock_);  
       filled.post(); 
    } 
 
    TID("Producer done"); 
} 
 
void* consumer(int nprod, int rank) { 
    TID("Starting consumer"); 
 
    for (int i = 0; i < nprod; i++) { 
       
  filled.wait(); 
omp_set_lock(&get_lock_); 
 int b = get(); 
usleep(1e2); 
       printf("%05d\n",b);  
omp_unset_lock(&get_lock_);  
       empty.post(); 
} 
 
    TID("Consumer done"); 
} 
 
int main(int argc, char *argv[]) 
{ 
    if(argc < 4 ){ 
        printf("Need three arguements num_products, num_producer, num_consumer.\n"); 
        return 1; 
    } 
    int num_product = atoi(argv[1]); 
    int np = atoi(argv[2]); 
    int nc = atoi(argv[3]); 
    if (omp_get_max_threads()!=np+nc) abort(); 
 
    omp_init_lock(&put_lock_);  
    omp_init_lock(&get_lock_); 
    #pragma omp parallel 
    { 
        #pragma omp single 
        {    
            for (int pIdx(0); pIdx<np; ++pIdx){ 
                #pragma omp task 
                producer(num_product, pIdx); 
            } 
 
            for (int cIdx(0); cIdx<nc; ++cIdx){ 
                #pragma omp task 
                consumer(num_product, cIdx); 
            } 
        } 
    } 
    omp_destroy_lock(&put_lock_);  
    omp_destroy_lock(&get_lock_);  
 
    return 0; 
} 

