#include <vector>
#include <mpi.h>

 typedef double data_t;

 class sparse {
 public:
 enum storage_type {CSR, COO};
 std::vector<data_t> data;
 std::vector<size_t> column;
 std::vector<size_t> row;

 size_t m,n; /* (local) matrix dimensions */
 storage_type type;
 sparse(size_t m, size_t n, storage_type t = CSR);
 void convertToCOO(sparse &B) const; /* returns transpose in B */
 void convertToCSR(sparse &B) const; /* returns transpose in B */
 void transpose(sparse &B) const; /* returns transpose in B */
 };
