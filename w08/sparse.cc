#include "sparse.h"
#include <cmath>
#include <algorithm>
#include <stdio.h>

 void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx){

   size_t n(0);
   idx.resize(v.size());
   std::generate(idx.begin(), idx.end(), [&]{ return n++; });
   std::sort(idx.begin(), idx.end(),[&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
 }

void swapRowAndColumn(sparse &B){
 std::vector<size_t> trow = B.row;
    B.row = B.column;
    B.column = trow;
}

sparse::sparse(size_t m, size_t n, storage_type t) :
     m(m), n(n), type(t) {}

void sparse::transpose(sparse &B) const{
    int size,rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
   // B.row.resize(B.m/size);
   /*     for(int i=0;i<B.row.size();i++){
    printf(" row i %d %d \n",i,B.row[i]);
    }
    */
    
   printf("rank is %d \n", rank);
   // printf("col size is %d \n ", B.column.size());
    convertToCOO(B);
     
   /* for(int i=0;i<B.column.size();i++){
     printf("prior i row col data %d %d %d %f \n",i,B.row[i],B.column[i],B.data[i]);
    }
    */
    swapRowAndColumn(B);

   /* for(int i=0;i<B.column.size();i++){
     printf("trans i row col data %d %d %d %f \n",i,B.row[i],B.column[i],B.data[i]);
    }
    */

    int rows_per_p = B.n/size;
    std::vector<size_t> rowprocno(B.row.size());
    for(int i=0; i<B.row.size(); i++){
        rowprocno[i] = B.row[i]/rows_per_p;
     //   printf(" dest i c val %d %d  \n",i,rowprocno[i]);
    }
    std::vector<size_t> idx;
    sort_index(rowprocno, idx);


    size_t row[B.row.size()];
    size_t column[B.column.size()];
    data_t data[B.data.size()];

   // filling send row, send col 
    for(int i=0; i<idx.size(); i++){
        row[i] = B.row[idx[i]]%rows_per_p;
        column[i] = B.column[idx[i]]+rank*B.m;
        data[i] = B.data[idx[i]];
   //     printf("send buff %d %d %d %f \n",i,row[i],column[i],data[i]);
    }
    
   //Computing send count
    std::vector<int> sendcount(size, 0);
    for(int i=0;i<idx.size();i++){
       sendcount[rowprocno[idx[i]]] += 1;
    }

    for(int i=0;i<size;i++){
        printf("send counts %d %d \n",i,sendcount[i]);      
    }
  
   // Calculating send offsets
    std::vector<int> sendoffset(size, 0);
    sendoffset[0] = 0;
    for(int i=1; i<size; i++){
        sendoffset[i] = sendoffset[i-1]+sendcount[i-1];
    }

    int recvcount[size];
    // getting recv count 
    MPI_Alltoall(sendcount.data(), 1, MPI_INT,recvcount, 1, MPI_INT, MPI_COMM_WORLD);

    int recvoffset[size];
    int count = recvcount[0];
    recvoffset[0]=0;
    
     //Computing recv offset
    for(int i=1; i<size; i++){
        recvoffset[i] = recvoffset[i-1]+recvcount[i-1];
        count += recvcount[i];
        printf("recv count %d %d \n",i,recvcount[i]);
    }
    
    std::vector<size_t> recvrow(count);
    std::vector<size_t> recvcolumn(count);
    std::vector<data_t> recvdata(count);

    MPI_Alltoallv(row, sendcount.data(), sendoffset.data(), MPI_UNSIGNED_LONG_LONG,
                  recvrow.data(), recvcount, recvoffset, MPI_UNSIGNED_LONG_LONG, MPI_COMM_WORLD);

    MPI_Alltoallv(column, sendcount.data(), sendoffset.data(), MPI_UNSIGNED_LONG_LONG,
                  recvcolumn.data(), recvcount, recvoffset, MPI_UNSIGNED_LONG_LONG, MPI_COMM_WORLD);

    MPI_Alltoallv(data, sendcount.data(), sendoffset.data(), MPI_DOUBLE,
                  recvdata.data(), recvcount, recvoffset, MPI_DOUBLE, MPI_COMM_WORLD);

    B.row.resize(count);
    B.column.resize(count);
    B.data.resize(count);
    
   for(int i=0;i<count;i++){
      B.row[i]= recvrow[i];
      B.column[i] = recvcolumn[i];
      B.data[i] = recvdata[i];
     // printf("recved row col data %d %d %d %f \n",i,B.row[i],B.column[i],B.data[i]);
    }

    sparse localFinal(rows_per_p,B.m*size);
    localFinal.row = std::vector<size_t>(B.row.size());
    localFinal.column = std::vector<size_t>(B.column.size());
    localFinal.data = std::vector<data_t>(B.data.size());

    sort_index(B.row, idx);
    for (int i = 0; i < idx.size(); i++) {
        localFinal.column[i] = B.column[idx[i]];
        localFinal.row[i] = B.row[idx[i]];
        localFinal.data[i] = B.data[idx[i]];
      printf("after coo localrow absoluterow col data  %d %d %d %f \n",localFinal.row[i],rank*rows_per_p+localFinal.row[i],localFinal.column[i],localFinal.data[i]);
    }
     
    for(int i=0;i<idx.size();i++){
     B.row[i]=localFinal.row[i];
     B.column[i]=localFinal.column[i];
     B.data[i]=localFinal.data[i];
    }
    
    convertToCSR(B);
/*    for(int i=0;i<=rows_per_p;i++){
           printf("after csr localrow absoluterow count  %d %d %d \n"  ,i,rank*rows_per_p+i,B.row[i]);
     }
    for(int i=0;i<idx.size();i++){
      printf("after csr col data  %d %d \n" ,localFinal.column[i],localFinal.data[i]);

    }
  */  
}


void sparse::convertToCOO(sparse &B) const{

        std::vector<size_t> row(B.data.size());
        std::vector<size_t> csrrow = B.row;
        int count = 0;
        for(int i=1;i<csrrow.size();i++){
                int val = csrrow[i]-csrrow[i-1];
                for(int j=0;j<val;j++){
                        row[count++] = i-1;
                }
        }
       // printf("Here  ");
        B.row.resize(B.data.size());

        for(int i=0;i<B.row.size();i++){
                B.row[i] = row[i];
        }

        B.type = COO;

}


void sparse::convertToCSR(sparse &B) const{

        std::vector<size_t> row;
        int count = 1;

        row.push_back(0);
        std::vector<size_t> coorow = B.row;

        for(int i=1;i<coorow.size();i++){
     if(coorow[i]==coorow[i-1]){
        count++;
     }
     else{
        row.push_back(count);
        count = count+1;
     }
        }
        row.push_back(count);
        B.row.resize(row.size());
        for(int i=0;i<=row.size();i++){
          B.row[i]=row[i];
        }
        B.type = CSR;
}



