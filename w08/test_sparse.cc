#include "sparse.h"
#include <stdio.h>
#include <mpi.h>
#include <cstdlib>

int main(int argc, char **argv)
{
	if (argc < 3) {
		printf("arguments should be m and n \n");
		return 1;
	}

	MPI_Init(&argc, &argv);
        int m = atoi(argv[1]);
        int n = atoi(argv[2]);
	int size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        std::vector<size_t> rows;
        std::vector<size_t> cols;
        std::vector<data_t> data;
        size_t rowsInProcess = m/size;
  
if (rank == 0) {
          int count = 0;
rows.push_back(0);
       // I have taken this code from piazza post 
       // https://piazza.com/class/jlce2tewgra4n?cid=65

              for ( size_t ii = 0, data_idx = 0; ii < m; ii++ )
        {
        
	for ( size_t jj = 0; jj < n; jj++ )
	{
		if ( 0 == rand() % 4 )
		{       int val = rand()%100;
			data.push_back(val);
			cols.push_back(jj);
			count = count +1;
			data_idx++;
                        printf("prior row col data %d %d %d \n ",ii,jj,val);
		}
	} 
          rows.push_back(count);
         // printf("row count i %d %d \n",ii,count);

}

}
        size_t count = 0;
        std::vector<int> send_count(size,0);
       if (rank == 0)  {
                for (int p = 0; p < size; p++)
                        send_count[p] = (int)(rows[(p+1)*rowsInProcess] - rows[p * rowsInProcess]);
        }
        MPI_Scatter(send_count.data(), 1, MPI_INT, &count, 1, MPI_INT, 0, MPI_COMM_WORLD);

        sparse A {rowsInProcess, n};
        A.data.resize(count);
        A.column.resize(count);
        A.row.resize(rowsInProcess + 1);
        
        std::vector<int> send_disp(size,0);
        if (rank == 0) {
                send_disp[0] = 0;
                for (int p = 1; p < size; p++) {
                        send_disp[p] = send_count[p - 1] + send_disp[p - 1];
                }
        }

        MPI_Scatterv(data.data(), send_count.data(),send_disp.data() , MPI_DOUBLE, A.data.data(), count, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv(cols.data(), send_count.data(),send_disp.data(), MPI_UNSIGNED_LONG_LONG, A.column.data(), count,
                MPI_UNSIGNED_LONG_LONG, 0, MPI_COMM_WORLD);
        MPI_Scatter(rows.data(), rowsInProcess, MPI_UNSIGNED_LONG_LONG, A.row.data(), rowsInProcess, MPI_UNSIGNED_LONG_LONG, 0,
                MPI_COMM_WORLD);

        A.row[rowsInProcess] = A.row[0] + count;
        A.transpose(A);

        MPI_Finalize();
}
