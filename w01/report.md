**Task 1.**

1) The search path for commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION  below).   A  zero-length  (null)
directory  name  in the value of PATH indicates the current directory.  A null directory name may appear as two adjacent colons, or as an initial or trailing colon.  The
default path is system-dependent, and is set by the administrator who installs bash.  A common value is ``/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin''.

2) A colon-separated list of directories in which to search for ELF libraries at execution-time. Similar to the PATH environment variable.

3) The value stored in HOME is /home/koch0165.

**Task 2.**

4) Completed

5)The PATH variable before loading intel module is "/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin".
The PATH variable after loading intel module is "/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin".
"/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin"  are appended to PATH variable after loading intel module

The LD_LIBRARY_PATH variable before loading intel module is "".
The LD_LIBRARY_PATH variable after loading intel module is "curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64".
"curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64" is appended to LD_LIBRARY_PATH after loading intel module

6) New modules are added under MPI Implementations and Compiler Dependent Applications  after loading intel module.

7)  In a hierarchical module system, only when all the dependencies have been satisfied Modules are available to be loaded. This prevents the loading of modules that are inconsistent with each other.

**Task 3.**

8) The environment variables set by gcc module are CURC_GCC_ROOT, CURC_GCC_LIB, CURC_GCC_INC, CURC_GCC_BIN, CC, FC, CXX.
The environment variales set by intel module are INTEL_LICENSE_FILE, CURC_INTEL_ROOT, CURC_INTEL_LIB, CURC_INTEL_INC, CURC_INTEL_BIN, CC, FC, CXX, AR, LD.

**Task 4.**

9)
The output when i ran gcc -v on my computer :
Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
Apple LLVM version 9.0.0 (clang-900.0.39.2)
Target: x86_64-apple-darwin17.4.0
Thread model: posix
InstalledDir: /Library/Developer/CommandLineTools/usr/bin


10) 
The commands use to make executable are :
icc -c axpy.cc
icc -o test_axpy.exe test_axpy.cc axpy.o

**Task 5.**

11) The content of .err file is empty.
    The content of .out file is - 
"Running on shas0140.rc.int.colorado.edu
Calling axpy - Passed"

The commands I used to monitor job are :
squeue -u $USER
scontrol show job {JOBID}

**Task 6.**

12) 
I received the following warnings after adding omp directives in file  if I compiled using icc :
axpy.cc(4): warning #3180: unrecognized OpenMP #pragma
       #pragma omp parallel
               ^
axpy.cc(6): warning #3180: unrecognized OpenMP #pragma
        #pragma omp for

Addition of -fopenmp flag while compiling removed the warnings.
The updated commands used to make executable are :
icc -fopenmp -c axpy.cc
icc -fopenmp -o test_axpy.exe test_axpy.cc axpy.o

The program execution time reduces by half from 0.8 s to 0.4 s after using omp.

13) The following are the results recorded :
[koch0165@shas0136 ~]$ OMP_NUM_THREADS=1 ./test_axpy.exe
Calling axpy - Passed
Elapsed time: 0.490144s
[koch0165@shas0136 ~]$ OMP_NUM_THREADS=2 ./test_axpy.exe
Calling axpy - Passed
Elapsed time: 0.482947s
[koch0165@shas0136 ~]$ OMP_NUM_THREADS=4 ./test_axpy.exe
Calling axpy - Passed
Elapsed time: 0.406745s
[koch0165@shas0136 ~]$ OMP_NUM_THREADS=8 ./test_axpy.exe
Calling axpy - Passed
Elapsed time: 0.305781s
[koch0165@shas0136 ~]$ OMP_NUM_THREADS=16 ./test_axpy.exe
Calling axpy - Passed
Elapsed time: 0.270213s

**Task 7.**

14) Completed
15) Completed
16) Completed
17) Completed


