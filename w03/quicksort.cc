#include "quicksort.h"
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include "omp-utils.h"
#include <math.h>

#define SWITCH_SIZE 4e4  

void scan_s(long *x, size_t n,long *y){
        y[0]= x[0];
    for(int i=1; i<n; i++) y[i]=x[i]+y[i-1];
}
// Implemented using pseudo code from class
void scan_p(const long* x, size_t n, long* y) {
    int depth = (int) ceil(log2(n)) + 1;
    long U[depth][n];
    long D[depth][n];

   # pragma omp parallel for if(n>SWITCH_SIZE) 
    for (int i = 0; i < n; i++)
        U[0][i] = x[i];

    for (int h = 1; h <= log2(n); h++) {
        long nelem = (int) floor(n / pow(2, h));

       #pragma omp parallel for if(n>SWITCH_SIZE)
        for (int j = 0; j < nelem; j++)
            U[h][j] = U[h - 1][2 * j] + U[h - 1][2 * j + 1];

    }

    for (int h = depth-1; h >= 0; h--) {
        D[h][0] = U[h][0];
        long nelem = (int) floor(n / pow(2, h));

        #pragma omp parallel for if(n>SWITCH_SIZE)
        for (int j = 1; j < nelem; j++) {
            D[h][j] = D[h + 1][(j - 1) / 2];
            if (j % 2 == 0) D[h][j] += U[h][j];
        }

    }     
         
   #pragma omp parallel for if(n>SWITCH_SIZE)
    for (int i = 0; i < n; i++)
        y[i] = D[0][i];

}            
      
int partition(long *y,int low,int high){
        int n = high-low+1;
        long lower[n], higher[n], ls[n], hs[n],lows[n],highs[n]; 
        int pivot = y[low];

    #pragma omp parallel for if(n>SWITCH_SIZE)
    for (size_t i = 0; i <n; ++i){
     lower[i] = y[i+low] < pivot;
      higher[i] = y[i+low] >= pivot;
    }

if(n>SWITCH_SIZE){
    scan_p(lower,n,ls);
    scan_p(higher, n,hs);
        }
   else{
    scan_s(lower,n,ls);
    scan_s(higher, n,hs);
        }

   size_t m = ls[n-1];
    #pragma omp parallel for if(n>SWITCH_SIZE)
    for (int i = 0; i < n; i++) {
        if (lower[i]) {
            lows[ls[i] - 1] = y[i+low];
        }else if(higher[i]){
            highs[hs[i]-1] = y[i+low];
        }
    }

    #pragma omp parallel for if(n>SWITCH_SIZE)
    for (int i=low;i<low+m;i++){
        y[i] = lows[i-low];
    }

    #pragma omp parallel for if(n>SWITCH_SIZE)
    for (int i=low+m;i<=high;i++){
        y[i] = highs[i-(low+m)];
    }

    return low+m;
}

void pQsort(long *x, int low, int high)
{
        int pindex;
        if(low < high)
        {
        pindex = partition(x, low, high);

#pragma omp task
{
    pQsort(x, low, pindex-1);
}

#pragma omp task
{
        pQsort(x, pindex+1, high);
}

}
}

void quicksort_cust(long *arr,int low, int high){

if(high-low+1<SWITCH_SIZE){
#pragma omp parallel                                                        
{
   #pragma omp single nowait
   pQsort(arr, low, high);
   }
}else{
   int pindex;
   pindex = partition(arr, low, high);
   quicksort_cust(arr, low, pindex-1);
   quicksort_cust(arr, pindex+1, high);
}
}

void quicksort(const vec_t &x, vec_t &y)
{
 int n =   x.size();
long *z;
z = new long[x.size()];
#pragma omp parallel for
for (int i = 0; i < n; ++i)
{
        z[i] = x[i];
}
quicksort_cust(z,0,n-1);
#pragma omp parallel for
for (int i = 0; i < n; ++i)
{
        y[i] = z[i];
}

}

                                                                                                                                                                                          120,1         67%
