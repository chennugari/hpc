#include <iostream>
#include <stdio.h>

long fib(long n) {
  long i(0), j(0);
 if (n<2) return 1;

 #pragma omp task shared(i)
 i = fib(n-1);

 #pragma omp task shared(j)
 j = fib(n-2);

 #pragma omp taskwait
 return i + j;
}

void demo_task(){
long n(5), v;
#pragma omp parallel shared (n, v)
{
#pragma omp single /* why single? */
v = fib(n);
}
printf("Fib val is %ld",v);
}

int main () {
   demo_task();
}
