#include <mpi.h> 
#include <stdio.h>  
#include "slurp_file.h"  
 
int main(int argc, char *argv[])  
{  
          if(argc < 3) 
        { 
                printf("Give file name and no of columns as argument \n"); 
                return 1; 
        } 
        int nofprocesses, rank;  
        MPI_Init(&argc, &argv);  
        MPI_Comm_size(MPI_COMM_WORLD, &nofprocesses);  
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);  
          
        std::vector<int> local_array; 
        std::vector<data_t> x_array;  
                
        int length = atoi(argv[2]);
        slurp_file_line(argv[1], length, x_array);
       
        int start = rank*(length/nofprocesses);
        int end = (rank+1)*(length/nofprocesses);
	//printf("The start is rank is  %d %d \n",start,rank); 

        int index =start;
        local_array.resize(length); 
        for(int i=0;i<length;i++){
           local_array[i] = 0;
        }
       
        while((rank==nofprocesses-1&&index<=length-1)||index<end){
            std::vector<data_t> first_array;  
            slurp_file_line(argv[1], index, first_array);

            for(int i=0;i<length;i++){  
                local_array[i] = local_array[i] + x_array[index]*first_array[i];
            }
          index++;
        } 
          //      printf("Computed local array rank %d \n",rank);  
        MPI_Status st;
       
        if(rank==0){
        std::vector<int> input; 
        input.resize(length);
        if(nofprocesses>1){
        MPI_Recv(&input[0], length, MPI_INT, rank+1, 0, MPI_COMM_WORLD, &st);
        }
        std::vector<int> final;
        final.reserve(length);
        printf("The final vector is\n");
        for(int i=0;i<length;i++){
            final[i] = local_array[i]+input[i];
            printf("%d \n", final[i]);
        } 
        }
        else if(rank == nofprocesses-1){
            MPI_Send(&local_array[0], length, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
         }
        else{
           // printf("Rank %d",rank);
             std::vector<int> input; 
             input.resize(length);
             MPI_Recv(&input[0], length, MPI_INT, rank+1, 0, MPI_COMM_WORLD, &st);
           //  printf("Received at rank %d from rank \n",rank,rank+1);
            std::vector<int> output;
                                 output.resize(length); 
            for(int i=0;i<length;i++){
             output[i] = local_array[i]+input[i];
           //  printf("input local out %d %d %d \n",input[i],local_array[i],output[i]);
            } 
            MPI_Send(&output[0], length, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
        }
        
        MPI_Finalize();  
}  
