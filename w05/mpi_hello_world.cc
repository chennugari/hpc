#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int nofprocesses, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nofprocesses);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	printf("Hello from processor %d of %d\n", rank, nofprocesses);
	MPI_Finalize();
}
