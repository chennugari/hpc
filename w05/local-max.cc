#include <mpi.h>
#include <stdio.h> 
#include "slurp_file.h" 

int main(int argc, char *argv[]) 
{       
        if(argc < 2)
	{
		printf("Give file name as argument \n");
		return 1;
	}
        
        int nofprocesses, rank; 
        MPI_Init(&argc, &argv); 
        MPI_Comm_size(MPI_COMM_WORLD, &nofprocesses); 
        MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
         
        printf("Hello from processor %d of %d\n", rank, nofprocesses); 
       
	std::vector<data_t> local_array;
        slurp_file_line(argv[1], rank, local_array);

        int localMax = 0;

        for(int i=0;i<local_array.size();i++){
     	  if(local_array[i]>localMax){
    		localMax = local_array[i];
    	}
        }

        printf("local max of line %d is %d\n", rank, localMax);
        MPI_Finalize(); 
} 
