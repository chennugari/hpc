#include <mpi.h> 
#include <stdio.h>  
#include "slurp_file.h"  
 
int main(int argc, char *argv[])  
{       
        //printf("no of arguments are %d",argc);
        if(argc < 2) 
        { 
                printf("Give file name as argument \n"); 
                return 1; 
        }  
        int nofprocesses, rank;  
        MPI_Init(&argc, &argv);  
        MPI_Comm_size(MPI_COMM_WORLD, &nofprocesses);  
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);  
          
        std::vector<data_t> local_array; 
        slurp_file_line(argv[1], rank, local_array); 

        int localMax = 0; 
 
        for(int i=0;i<local_array.size();i++){ 
          if(local_array[i]>localMax){ 
                localMax = local_array[i]; 
        } 
        } 

        MPI_Status st;
        if(rank==0){
        if(nofprocesses>1){        
        int b;
        MPI_Recv(&b, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD, &st);
        int globalMax = b>localMax?b:localMax;
        printf("global max  is %d\n", globalMax); 
        }
        else{
        printf("global max  is %d\n", localMax);  
         }
        }
        else if(rank == nofprocesses-1){
            MPI_Send(&localMax, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
        }
        else{
        
            int b;
            MPI_Recv(&b, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD, &st);
            int maxhere = b>localMax?b:localMax;
            MPI_Send(&maxhere, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
        
        }
 
        MPI_Finalize();  
}  
