/**
 *  *  * @file
 *   *   * @author Rahimian, Abtin <arahimian@acm.org>
 *    *    * @revision $Revision: 25 $
 *     *     * @tags $Tags: tip $
 *      *      * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *       *       */

#include "poisson.h"
#include <stdio.h>  
#include <iostream>
#include <math.h>
void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{

	int nodes;
	int rank;

    MPI_Comm_size(comm, &nodes);
    MPI_Comm_rank(comm, &rank);

    int dimensions[] = {0, 0, 0};
    int periods[] = {0, 0, 0};
    int coordinates[3];

    MPI_Dims_create(nodes, 3, dimensions);
    MPI_Cart_create(comm, 3, dimensions, periods, 1, &grid_comm);
    MPI_Cart_coords(grid_comm, rank, 3, coordinates);


    double iwidth = n/dimensions[0];
    double jwidth = n/dimensions[1];
    double kwidth = n/dimensions[2];

    int ilim = (coordinates[0]+1)*iwidth<n?(coordinates[0]+1)*iwidth:n;
    int jlim = (coordinates[1]+1)*jwidth<n?(coordinates[1]+1)*jwidth:n;
    int klim = (coordinates[2]+1)*kwidth<n?(coordinates[2]+1)*kwidth:n;

    for(double i=coordinates[0]*iwidth; i< ilim; i++){
    for(double j=coordinates[1]*jwidth; j<jlim; j++){
    for(double k=coordinates[2]*kwidth; k< klim; k++){
                 point_t point; 
                 point.x = i; 
                 point.y = j; 
                 point.z = k; 
                 x.push_back(point);
                            }
			}
		   }
                                                                                       
}

int index_val(double i, double j, double k, double iRange,double jRange)
{
	return (int)(k + j*jRange + i*iRange*jRange);
}

double val_after_cart_shift(const MPI_Comm &grid_comm, int dimension, int  shift, double val)
{
	int source, destination;
        double recvval ;
	MPI_Status status;
	MPI_Cart_shift(grid_comm, dimension, shift,  &source, &destination);	
	if(destination != MPI_PROC_NULL)
	{
		MPI_Send(&val, 1, MPI_DOUBLE, destination, 0, grid_comm);
	}

	MPI_Cart_shift(grid_comm, dimension , shift * -1, &source, &destination);
	if(destination != MPI_PROC_NULL)
	{
		MPI_Recv(&recvval, 1, MPI_DOUBLE, destination, 0, grid_comm, &status);
		return recvval-val;
	}

	return 0;
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{

    int dimensions[] = {0, 0, 0}; 
    int periods[] = {0, 0, 0}; 
    int coordinates[] = {0,0,0};

    MPI_Cart_get(grid_comm, 3, dimensions, periods, coordinates);

    int iwidth = n/dimensions[0];
    int jwidth = n/dimensions[1];
    int kwidth = n/dimensions[2];

    int ilim = (coordinates[0]+1)*iwidth<n?iwidth:n-(coordinates[0])*iwidth; 
    int jlim = (coordinates[1]+1)*jwidth<n?jwidth:n-(coordinates[1])*jwidth; 
    int klim = (coordinates[2]+1)*kwidth<n?kwidth:n-(coordinates[2])*kwidth; 

    double h = 1.0/((double)n-1.0);

    for(int i=0; i<ilim; i++){
       for(int j=0; j<jlim; j++){
           for(int k=0; k<klim; k++){
                double lv_val = 0;

                int index = index_val(i, j, k, ilim, jlim);

                double vval = v[index];
                double aval = a[index];

                if(i==0) {
                    lv_val += val_after_cart_shift( grid_comm, 0, -1,vval);
                    lv_val += v[index_val(i+1, j, k, ilim, jlim)]-vval;
                }else if(i==ilim-1){
                    lv_val += val_after_cart_shift( grid_comm, 0, 1,vval);
                    lv_val += v[index_val(i-1, j, k, ilim, jlim)]-vval;
                }else{
                    lv_val += v[index_val(i+1, j, k, ilim, jlim)]-vval;
                    lv_val += v[index_val(i-1, j, k, ilim, jlim)]-vval;
                }

                if(j==0) {
                    lv_val += val_after_cart_shift( grid_comm, 1, -1,vval);
                    lv_val += v[index_val(i, j+1, k, ilim, jlim)]-vval;
                }else if(j==jlim-1){
                    lv_val += val_after_cart_shift( grid_comm, 1, 1,vval);
                    lv_val += v[index_val(i, j-1, k, ilim, jlim)]-vval;
                }else{
                    lv_val += v[index_val(i, j+1, k, ilim, jlim)]-vval;
                    lv_val += v[index_val(i, j-1, k, ilim, jlim)]-vval;
                }

                if(k==0) {
                    lv_val += val_after_cart_shift( grid_comm, 2, -1,vval);
                    lv_val += v[index_val(i, j, k+1, ilim, jlim)]-vval;
                }else if(k==klim-1){
                    lv_val += val_after_cart_shift( grid_comm, 2, 1,vval);
                    lv_val += v[index_val(i, j, k-1, ilim, jlim)]-vval;
                }else{
                    lv_val += v[index_val(i, j, k+1, ilim, jlim)]-vval;
                    lv_val += v[index_val(i, j, k-1, ilim, jlim)]-vval;
                }
                lv_val = aval*vval - lv_val/(h*h);
                lv[index] = lv_val;

            }
        }
    }

}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
    
     int rank;  
  
    MPI_Comm_rank(comm, &rank); 

    vec_t lv;
    lv.reserve(v.size()); 
    mv(v,lv);
    res.reserve(v.size());
    double totalResidueInProc = 0;
	for(int i=0; i<v.size(); i++){ 
           	    res[i] = rhs[i] - lv[i];
           	    totalResidueInProc+= res[i]*res[i]; 
           }
   	MPI_Barrier(comm);

	MPI_Allreduce(&totalResidueInProc, &res_norm, 1, MPI_DOUBLE, MPI_SUM,comm);

        res_norm = sqrt(res_norm);
       /* if(rank==0){
        printf("res_norm %f \n",res_norm);
        }*/
}

/*
void jacobi_solver(const MPI_Comm &comm, matvec_t &mv,
std::vector<real_t> &diag, std::vector<real_t> &rhs,
std::vector<real_t> &u0, real_t r0, real_t eps_r, real_t eps_a, int k_max,
std::vector<real_t> &u_final, int &k){
   
   vec_t res;
   real_t res_norm;
   res.reserve(u0.size());

   residual(comm,mv,u0,rhs,res,res_norm);

   if(k>k_max||res_norm<eps_r*r0+eps_a){
    for(int i=0;i<u0.size();i++){
        u_final[i] = u0[i];
    }
    return;
   }

    for(int i=0;i<u0.size();i++){
        u0[i] = u0[i] + diag[i]*res[i];
    }

    k++; 
    printf("k %d \n", k);
    MPI_Barrier(comm); 
    jacobi_solver(comm,mv,diag,rhs,u0,r0,eps_r,eps_a,k_max,u_final,k);

}
*/
   




