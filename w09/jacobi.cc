#include "poisson.h"
#include<stdio.h>

void jacobi_solver(const MPI_Comm &comm, matvec_t &mv,
std::vector<real_t> &diag, std::vector<real_t> &rhs,
std::vector<real_t> &u0, real_t r0, real_t eps_r, real_t eps_a, int k_max,
std::vector<real_t> &u_final, int &k){
    int rank;

    MPI_Comm_rank(comm, &rank);

   vec_t res;
   real_t res_norm;
   res.reserve(u0.size());
   
   residual(comm,mv,u0,rhs,res,res_norm);
  // res_norm = res_norm/(n*n*n);
   if(rank==0){
    printf("res_norm %f",res_norm);
     printf("k %d \n", k);
   } 

  if(k>k_max||res_norm<eps_r*r0+eps_a){
    for(int i=0;i<u0.size();i++){
        u_final[i] = u0[i];
    }
    return;
   }

    for(int i=0;i<u0.size();i++){
        u0[i] = u0[i] + diag[i]*res[i];
    }

    k++;
   // printf("k %d \n", k);
    MPI_Barrier(comm);
    jacobi_solver(comm,mv,diag,rhs,u0,r0,eps_r,eps_a,k_max,u_final,k);

}
