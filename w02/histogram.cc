#include "histogram.h" 
#include <cstdio> 
#include <cassert> 
#include "omp-utils.h" 
 
void histogram(const vec_t &x, int num_bins, 
                   vec_t &bin_bdry, count_t &bin_count){ 
 
        bin_bdry.reserve(num_bins+1); 
        bin_count.reserve(num_bins); 
        int n = x.size(); 
 
    double min_val(x[0]),max_val(x[0]); 
    double bin_size = x[0]; 
 
 
    #pragma omp parallel shared(min_val,max_val) 
    { 
        #pragma omp for reduction(max:max_val) reduction(min: min_val) 
                for(int i=0; i < n; i++) 
                { 
                        if(x[i] > max_val) 
                        { 
                        max_val = x[i]; 
                        } 
                        if(x[i] < min_val) 
                        { 
                        min_val = x[i]; 
                        } 
                } 
 
         #pragma omp single 
        { 
          printf("minval %lf" , min_val); 
          printf("maxval %lf" , max_val); 
          bin_size = (max_val-min_val)/num_bins; 
          max_val = max_val*(1+1e-14); 
          bin_bdry[0] = min_val*(1- 1e-14); 
          bin_bdry[num_bins]=max_val; 
        } 
 
        #pragma omp for 
        for (int i=0; i<num_bins; i++){ 
                if(i>0) 
            bin_bdry[i] = min_val + bin_size*i; 
        } 
 
        size_t privatecount[num_bins]; 
        for (int i = 0; i < num_bins; i++) 
                        privatecount[i] = 0; 
 
        #pragma omp for 
        for (size_t i = 0; i < n; i++) { 
            int bin_pos = (int)((x[i] - min_val) / bin_size); 
            if(bin_pos==num_bins){ 
                bin_pos=num_bins-1; 
            } 
            privatecount[bin_pos] += 1; 
 
        } 

                #pragma omp critical 
        { 
             for (int i = 0; i < num_bins; i++){ 
                bin_count[i] += privatecount[i]; 
            } 
        } 
    } 
    return; 
} 
